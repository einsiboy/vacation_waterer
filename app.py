from flask import Flask, render_template, request, jsonify

from WateringSystem import WateringSystem

app = Flask(__name__)

SYSTEM_IDS = [1]
POTS_PER_SYSTEM = [2]
ip = "192.168.1.122"
g_watering_system = WateringSystem(ip)


@app.route("/")
def hello():
    return render_template('index.html')


@app.route("/get-system-info", methods=["GET"])
def poll_sysinfo():
    res = g_watering_system.poll_info_from_system()
    return res
    # return jsonify(res.content.decode())
    # return jsonify(res.decode())


@app.route("/manual-watering", methods=["POST"])
def manual_watering():
    print("request", request)
    print("VOLUME", request.values["volume"])

    # use system_id if multiple systems
    system_id = request.values["systemID"]
    d = {
        "volume": request.values["volume"],
        "potID": request.values["potID"]
    }

    res = g_watering_system.send_watering_instruction(d)
    return jsonify(res.decode())

@app.route("/set-threshold", methods=["POST"])
def set_threshold():
    # use system_id if multiple systems
    system_id = request.values["systemID"]
    d = {
        "threshold": request.values["threshold"],
        "potID": request.values["potID"]
    }
    res = g_watering_system.set_threshold(d)
    return jsonify(res.decode())




if __name__ == '__main__':
    app.run()
