"""
A class for the vacation watering system.
"""
import json

import requests


class WateringSystem:
    def __init__(self, ip):
        self.ip = ip
        self.url = "http://" + self.ip

    def send_watering_instruction(self, data):
        r = requests.post(self.url + "/manual-water", data)

        if r.status_code == 200:
            return r.content

    def set_threshold(self, data):
        r = requests.post(self.url + "/set-threshold", data)

        if r.status_code == 200:
            return r.content

    def poll_info_from_system(self):
        r = requests.get("http://" + self.ip + "/system-info", timeout=1.5)

        if r.status_code == 200:
            j = json.loads(r.content.decode())

            for pot in j:
                pot["averageMoisture"] = sum(pot["moisture"]) / len(pot["moisture"])

            return json.dumps(j)
            # return json.loads(r.content.decode())

        return None

    # def read_pot_moisture(self, pot_id):
    #     system_info = self.poll_info_from_system()
    #     return system_info
    #     # TODO extract for only pot of interest


def sysInfo():
    ip = '192.168.1.122'
    r = requests.get("http://" + ip + "/system-info")

    print(r.status_code)

    if r.status_code == 200:
        print(r.content)
        return r.content
