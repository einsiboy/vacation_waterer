$(document).ready(function () {

    $("#relay-1").change(function (e) {
        console.log("relay one on: ", this.checked);
    });

    // $('form').on('submit', function(event) {
    //
    // 	$.ajax({
    // 		data : {
    // 			name : $('#nameInput').val(),
    // 			email : $('#emailInput').val()
    // 		},
    // 		type : 'POST',
    // 		url : '/process'
    // 	})
    // 	.done(function(data) {
    //
    // 		if (data.error) {
    //             alert(data.error)
    // 		}
    // 		else {
    // 		    console.log(data);
    // 		}
    //
    // 	});
    //
    // 	event.preventDefault();
    //
    // });

    $('form.set-threshold').on('submit', function (e) {
        e.preventDefault();

        const systemID = $(this).find("input.system-id").val();
        const potID = $(this).find("input.pot-id").val();
        const threshold = $(this).find("input.input-theshold").val();
        let _this = this;


        const dat = {
            "threshold": threshold,
            "systemID": systemID,
            "potID": potID
        }
        console.log(dat);


        $.post("/set-threshold", dat).done(function (data) {
            console.log("data", data);
            // $(_this).find("input").val("success")
            toastr.success("updated threshold", 'Pot' + " 1",
                {positionClass: "toast-top-center", timeout: 500})

            updateSystemInfo();
        }).fail(function (response) {
            toastr.error("failed to set threshold");
            console.log('Error: ' + response.responseText);
        });
    })


    $('form.manual-water').on('submit', function (e) {

        const systemID = $(this).find("input.system-id").val();
        const potID = $(this).find("input.pot-id").val();
        const volume = $(this).find("input.water-amount").val();
        let _this = this;


        const dat = {
            "volume": volume,
            "systemID": systemID,
            "potID": potID
        }
        console.log(dat);


        $.post("/manual-watering", dat).done(function (data) {
            console.log("data", data);
            // $(_this).find("input").val("success")
            toastr.success("with " + volume + " liters", 'Watering pot' + " 1",
                {positionClass: "toast-top-center"})
        }).fail(function (response) {
            console.log('Error: ' + response.responseText);
        });

        e.preventDefault();
    })

    function updateSystemInfo() {
        $("#btn-poll-sysinfo").click();
    }

    $("#btn-poll-sysinfo").on("click", function (e) {
        console.log("get moisture");
        console.log(e);
        console.log(this);

        $.get("/get-system-info").done(function (data) {
            console.log("read moisture, data:");
            console.log(data);
            data = JSON.parse(data);

            let currMoistureTags = $(".current-moisture");
            let thresholdTages = $(".moisture-threshold");

            for (let i = 0; i < data.length; i++) {
                let d = data[i];

                console.log(d);
                $(currMoistureTags[i]).text("Current moisture: " + d["averageMoisture"])
                $(thresholdTages[i]).text("Moisture threshold: " + d["threshold"])

            }

            toastr.success("updated system info", "", {timeOut: 450, positionClass: "toast-top-center"});
        }).fail(function (response) {
            toastr.error("could not get moisture level", "",
                {timeOut: 750, positionClass: "toast-top-center"})
        })
    })


    /**
     * plotly stuff
     */
    var trace1 = {
        x: [1, 2, 3, 4],
        y: [10, 15, 13, 17],
        type: 'scatter'
    };

    var trace2 = {
        x: [1, 2, 3, 4],
        y: [16, 5, 11, 9],
        type: 'scatter'
    };

    var data = [trace1, trace2];

    Plotly.newPlot('plotly-system-1-div', data);

    updateSystemInfo();
});

