/*
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com/esp32-client-server-wi-fi/

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files.

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
*/


/**
   Followed this to store wifi credentials:
   https://www.megunolink.com/articles/wireless/how-do-i-connect-to-a-wireless-network-with-the-esp32/
*/


// Import required libraries
#include "WiFiConfig.h"
#include "WiFi.h"
#include "ESPAsyncWebServer.h"


IPAddress localIP(192, 168, 1, 122);
const double ML_PER_MS = 0.00075;  // would need more testing for this


char buf[1024];  // for communicating over wifi

// Create AsyncWebServer object on port 80
AsyncWebServer server(80);


enum DEBUG_LEVELS {NONE, INFO, DEBUG};
enum DEBUG_LEVELS DEBUG_LEVEL = INFO;



typedef struct {
  int n_moistureSensors;
  int moistureSensors[4];  // support up to 4 moisture sensors
  int pump;
  //  char *buf;
  int potID;  // just used for debug print commands


  // ====== watering
  float moistureThreshold = 20;

  double baseWateringVolume = 10.0; // in ml (500 would probably be a senhsible amount)
  double targetWateringVolume;  // updated each time we begin watering
  double wateredVolume = 0.0;  // keep track of how much have watered this cycle.

  //  size_t baseCoolDownTime = 60 * 60 * 1000 * 1000; // time in micro seconds, an hour should be fine?
  int baseCoolDownTime = 8 * 1000 * 1000;
  int coolDownTime = 0;

  size_t lastTime = esp_timer_get_time();

  float getMoistureFromSensor(int sensorIdx) {
    return (analogRead(moistureSensors[sensorIdx]) / 4095.0f) * 100;
  }

  void beginWatering(double volume_ml) {

    targetWateringVolume = volume_ml;
    wateredVolume = 0.0;
    if (DEBUG_LEVEL >= INFO) {
      printf("---------- beginning watering cycle, for pot: %d ---------------------\n", potID);
      printf("pot %d, in begin targetWateringVolume: %f\n", potID, targetWateringVolume);
    }
    digitalWrite(pump, HIGH);  // turn pump on
  }

  void stopWatering() {
    if (DEBUG_LEVEL >= INFO)
      printf("======================== stopping watering cycle, for pot: %d ==================== \n", potID);
    digitalWrite(pump, LOW);  // turn pump on
    coolDownTime = baseCoolDownTime;
    //    wateredVolume = 0.0;
  }

  void updateWateredVolume(size_t elapsed_us) {
    size_t elapsed_ms = elapsed_us / 1000;
    float loopWateredVolume = ML_PER_MS * elapsed_ms;
    wateredVolume += loopWateredVolume;

    if (DEBUG_LEVEL >= DEBUG)
      printf("pot %d, elapsed_ms: %ul, loopWateredVolume %f, wateredVolume: %f, targetWateringVolume: %f \n", potID, elapsed_ms, loopWateredVolume, wateredVolume, targetWateringVolume);
  }

  bool amWatering() {
    bool b = digitalRead(pump);
    if (DEBUG_LEVEL >= DEBUG)
      printf("pot %d, amWatering: %d: \n", potID, b);

    return b;
  }

  float getAverageMoisture() {
    float sum = .0;
    for (int i = 0; i < n_moistureSensors; ++i) {
      float val = getMoistureFromSensor(i);
      if (DEBUG_LEVEL >= DEBUG)
        printf("DEBUG pot %d, (sensor, pin): (%d, %d) val: %f\n", potID, i, moistureSensors[i], val);

      sum += val;
    }
    return sum / n_moistureSensors;
  }

  bool shouldWater() {
    float potMoisture = getAverageMoisture();
    bool b = (potMoisture < moistureThreshold && coolDownTime <= 0);
    if (DEBUG_LEVEL >= INFO)
      printf("pot %d, potMoisture: %f, moistureThreshold: %f, shouldWater: %d \n", potID, potMoisture, moistureThreshold, b);

    return b;
  }

  void waterUpdate() {
    // TODO return something indicating the system status
    // e.g. AM_WATERING, COOLING_DOWN, SUCCESS
    //
    //    if (DEBUG_LEVEL > NONE)
    //      Serial.println();
    if (DEBUG_LEVEL >= DEBUG)
      printf("---begin POT: %d ---\n", potID);
    //    printf("pot %d, targetWateringVolume: %f\n", potID, targetWateringVolume);

    size_t now = esp_timer_get_time();
    size_t elapsed = now - lastTime;
    lastTime = now;

    if (amWatering()) {
      if (DEBUG_LEVEL >= DEBUG)
        printf("Pot %d is currently watering\n", potID);

      updateWateredVolume(elapsed);
      if (wateredVolume >= targetWateringVolume) {
        if (DEBUG_LEVEL >= DEBUG)
          printf("pot %d about to stop watering. wateredVolume: %f, targetWateringVolume: %f\n", potID, wateredVolume, targetWateringVolume);

        stopWatering();
      }
      return;
    }

    if (shouldWater()) {
      beginWatering(baseWateringVolume);
      return;
    }

    if (coolDownTime > 0) {  // if in cooldown period
      if (DEBUG_LEVEL >= DEBUG)
        printf("pot %d in coolDown period, be patient, coolDownTime: %d\n", potID, coolDownTime);
      //      decreaseCooldownTime();

      coolDownTime -= elapsed;
      return;
    }

    //    printf("pot %d, targetWateringVolume: %f\n", potID, targetWateringVolume);
    if (DEBUG_LEVEL >= DEBUG)
      printf("--- end POT: %d ---\n", potID);
  }

  void manualWatering(float volume) {
    if (!amWatering()) {
      beginWatering(volume);
    }
  }

} Pot;


const int N_POTS = 2;
Pot pots[N_POTS];  // 2 to showcase multiple systems per esp32 device


// ===========================================================
// ===========================================================
// ===========================================================

void connectToWiFi()
{

  WiFi.mode(WIFI_STA);
  //  WiFi.config(localIP);

  WiFi.begin(SSID, WiFiPassword);
  Serial.print("Connecting to "); Serial.println(SSID);

  uint8_t i = 0;
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print('.');
    delay(500);

    if ((++i % 16) == 0)
    {
      Serial.println(F(" still trying to connect"));
    }
  }

  Serial.print(F("Connected. My IP address is: "));
  Serial.println(WiFi.localIP());


  //  WiFi.config();
  if (!WiFi.config(localIP, WiFi.gatewayIP(), WiFi.subnetMask())) {
    Serial.println("failed to config...");
  }

  Serial.print(F("Connected. My IP address is: "));
  Serial.println(WiFi.localIP());

}

//float getSystemMoisture() {
//  // the soil moisture sensor should return an int value between 0 and 4095
//  // just assume linear relationship between moisture % and analaog value read
//  return analogRead(moistureSensor) / 4095.0f;
//}

//float getMoisture(int sensor) {
//  return (analogRead(sensor) / 4095.0f) * 100;
//}


String readTemp() {
  return String("35");
}


char* systemInfo() {
  // return info for complete system (all pots)
  Serial.println("----- getting system info -----");
  int n = 0;

  n += sprintf(buf + n, "[");
  for (int i = 0; i < N_POTS; ++i) {
    n += sprintf(buf + n, "{\"potID\": %d, ", i);
    n += sprintf(buf + n, " \"moisture\": [");

    Pot pot = pots[i];

    // ====== moisture levels ====
    for (int j = 0; j < pot.n_moistureSensors - 1; j++) {
      n += sprintf(buf + n, "%.0f, ", pot.getMoistureFromSensor(j));
    }
    n += sprintf(buf + n, "%.0f]", pot.getMoistureFromSensor(pot.n_moistureSensors - 1));

    // ===== moisture threshold ====
    n += sprintf(buf + n, ", \"threshold\": %.0f ", pot.moistureThreshold);

    
    n += sprintf(buf + n, "}");

    if (i + 1 < N_POTS)
      n += sprintf(buf + n, ",");

  }
  n += sprintf(buf + n, "]");


  //    n = sprintf(buf, "{\"moisture\": [");


  //    int n;
  //    n = sprintf(buf, "{\"moisture\": [");
  //    for (int i = 0; i < N_MOISTURE_SENSORS - 1; i++) {
  //        n += sprintf(buf + n, "%.0f, ", getMoisture(moistureSensors[i]));
  //    }
  //    n += sprintf(buf + n, "%.0f]}", getMoisture(moistureSensors[N_MOISTURE_SENSORS - 1]));
  //
  return buf;
  //  return "abc\r\n";
}


void handleAutoWatering() {
  for (int i = 0; i < N_POTS; ++i) {
    Pot &pot = pots[i];
    pot.waterUpdate();
  }
}


char* onManualWater(AsyncWebServerRequest *request) {
  Serial.println("manually watering");

  // volume
  AsyncWebParameter* p = request->getParam("volume", true);
  float vol = p->value().toFloat();

  // system pot id
  p = request->getParam("potID", true);
  int potIdx = p->value().toInt();

  printf("beginning manual watering for (potIdx, potID): %d, %d, for %f liters\n", potIdx, pots[potIdx].potID, vol);
  pots[potIdx].manualWatering(vol);

  return "abc";
}

char* onSetThreshold (AsyncWebServerRequest *request) {
  Serial.println("Setting threshold manually");

  // volume
  AsyncWebParameter* p = request->getParam("threshold", true);
  float threshold = p->value().toFloat();

  // system pot id
  p = request->getParam("potID", true);
  int potIdx = p->value().toInt();

  printf("setting threshold manually (potIdx, potID): %d, %d, to %f \n", potIdx, pots[potIdx].potID, threshold);
  pots[potIdx].moistureThreshold = threshold ;

  return "abc";
}


// ==========================================================================
// =============== setup and loop ===========================================
// ==========================================================================

void setup() {
  // Serial port for debugging purposes
  Serial.begin(115200);
  Serial.println();


  /**
     Setup watering system per pot
  */


  //  char buf[1024];

  /**
     Pot 1 setup
  */

  Pot pot1;
  //    int pot1_moistureSensors[4] = {34, 35};
  pot1.moistureSensors[0] = 34;
  pot1.moistureSensors[1] = 35;

  pot1.potID = 1;
  pot1.n_moistureSensors = 2;
  pot1.pump = 32;
  //    pot1.buf = pot1_buf;
  pot1.moistureThreshold = 25;

  pots[0] = pot1;
  pinMode(pot1.pump, OUTPUT);
  //


  /**
     Pot 2 setup
  */

  Pot pot2;

  pot2.moistureSensors[0] = 36;

  pot2.potID = 2;
  pot2.n_moistureSensors = 1;
  pot2.pump = 19;
  pot2.moistureThreshold = 25;

  pinMode(pot2.pump, OUTPUT);

  pots[1] = pot2;


  // Setting the ESP as an access point
  //  Serial.print("Setting AP (Access Point)…");
  //  // Remove the password parameter, if you want the AP (Access Point) to be open
  //  //  IPAddress IP = WiFi.softAPIP();
  //
  //  WiFi.softAP(ssid, password);
  //  delay(100);         // Give AP some time to init
  //
  //  WiFi.softAPConfig(myip, myip, subnet);
  //  delay(500);
  //
  //  IPAddress IP = WiFi.softAPIP();
  //  Serial.print("AP IP address: ");
  //  Serial.println(IP);


  //  pinMode(moistureSensor, INPUT);

  /* =========================================
     =============== WIFI STUFF ==============
     =========================================
  */
  connectToWiFi();



  server.on("/temperature", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send_P(200, "text/plain", readTemp().c_str());
  });

  server.on("/system-info", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send_P(200, "text/plain", systemInfo());
  });

  server.on("/manual-water", HTTP_POST, [](AsyncWebServerRequest * request) {
    //    request->send_P(200, "text/plain", systemInfo());
    onManualWater(request);
    request->send_P(200, "text/plain", "manually watering done");
  });

    server.on("/set-threshold", HTTP_POST, [](AsyncWebServerRequest * request) {
    //    request->send_P(200, "text/plain", systemInfo());
    onSetThreshold(request);
    request->send_P(200, "text/plain", "manually watering done");
  });
  

  // Start server
  server.begin();
}

void loop() {
  //  moistureValue = analogRead(moistureSensor);
  //  moistureValue = moistureValue/10;
  //  Serial.println(moistureValue);


  //  Pot pot = pots[0];
  //  digitalWrite(pot.pump, !digitalRead(pot.pump));
  //  delay(1000);


  handleAutoWatering();
  //  Serial.println();
  //  printf("36: %d\n", analogRead(36));
  //  printf("34: %d\n", analogRead(34));
  //  printf("35: %d\n", analogRead(35));
  delay(2000);
}
